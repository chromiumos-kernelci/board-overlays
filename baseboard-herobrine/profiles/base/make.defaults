# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Initial value just for style purposes.
LINUX_FIRMWARE=""
USE=""
FW_BLOBS=""

USE="${USE} eclog"
USE="${USE} powerd_manual_eventlog_add"
USE="${USE} -opengl"
USE="${USE} opengles"
USE="${USE} touchview"
USE="${USE} watchdog"

# Enable TPM2 for H1
USE="${USE} -tpm tpm2"

# Include prebuilt (&signed) CR50 FW
USE="${USE} cr50_onboard"

# Enable KVM
USE="${USE} kvm_host crosvm-gpu virtio_gpu"

# Include the TCPC firmware binaries
FW_BLOBS="${FW_BLOBS} ps8805/ps8805_a2.bin"
FW_BLOBS="${FW_BLOBS} ps8805/ps8805_a3.bin"

# Google USBPD peripheral firmwares
LINUX_FIRMWARE="${LINUX_FIRMWARE} cros-pd"

# Firmware for WCN3991 Bluetooth
LINUX_FIRMWARE="${LINUX_FIRMWARE} qca-wcn6750-bt"

# Adreno 660 firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} adreno-660"

# Venus VPU 2.0 firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} venus-vpu-2"

# Firmware for WCN6750 WiFi
LINUX_FIRMWARE="${LINUX_FIRMWARE} ath11k_wcn6750"

# For minigbm, mesa driver selected by virtuals
VIDEO_CARDS="msm"

# Declare set of enabled consoles
TTY_CONSOLE="ttyMSM0"

# Enable Downloadable Content (DLC) Test
USE="${USE} dlc_test"

# Enable NVMe utility
USE="${USE} nvme"

# Enable eMMC tools
USE="${USE} mmc"

# WiFi 6E requires SAE H2E
USE="${USE} sae_h2e"

# Enable Type-C daemon
USE="${USE} typecd"

# Set mosys_platform
USE="${USE} -mosys_platform_generic mosys_platform_herobrine"

# Use modemfwd to load modem fw and start rmtfs
USE="${USE} modemfwd"

# Enable LVM stateful partition.
USE="${USE} lvm_stateful_partition"

# Enable LVM application containers.
USE="${USE} lvm_application_containers"

# Enable miniOS support
USE="${USE} minios"
